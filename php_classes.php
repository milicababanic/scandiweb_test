<?php

abstract class Product{

	private $type;
	public $sku;
    public $name;
	public $price;
	public $props;
	
	function set_data($type, $sku, $name, $price, $props) {
		$this->type = $type;
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->props = $props;
	}
	function get_type() {
		return $this->type;
	}
	function get_sku() {
		return $this->sku;
	}
	function get_name() {
		return $this->name;
	}
	function get_price() {
		return number_format($this->price,2)." $";
	}
	function get_props() {
		return $this->props;
	}

    abstract public function product_props(): string;
}

class DVD extends Product {
	public function product_props(): string{
		return "Size: $this->props MB";
	}
}

class Book extends Product {
	public function product_props(): string{
		return "Weight: $this->props KG";
	}
}

class Furniture extends Product {
	public function product_props(): string{
		return "Dimension: $this->props";
	}
}
?>