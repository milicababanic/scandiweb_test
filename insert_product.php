<div class="form_row">
    <div class="label">
        <label for="sku">SKU</label>
    </div>
    <div class="input">
        <input type="text" id="sku" name="sku" required minlength="8" maxlength="12">
        <span class="error" aria-live="polite"></span>
    </div>
</div>
<div class="form_row">
    <div class="label">
        <label for="name">Name</label>
    </div>
    <div class="input">
        <input type="text" id="name" name="name" required minlength="5" maxlength="12">
        <span class="error" aria-live="polite"></span>
    </div>  
</div> 
<div class="form_row">
    <div class="label">  
        <label for="price">Price &#40;&#36;&#41;</label>
    </div>
    <div class="input">
        <input type="number" id="price" name="price" min="1" max="1000" step="0.01">
        <span class="error" aria-live="polite"></span>
    </div>
</div>
<div class="type_switcher_container">
    <div class="type_switcher">
        <p>Type Switcher</p>
    </div>
    <div class="product_type_dropdown">
        <select name="Type_Switcher" id="productType" required onchange="showForm(this)">
            <option value="" selected disabled hidden>Type Switcher</option>
            <option value="DVD">DVD</option>
            <option value="Furniture">Furniture</option>
            <option value="Book">Book</option>
        </select>
    </div> 
</div>
<div id="DVD"class="df_row">
    <div class="label">  
        <label for="size">Size &#40;MB&#41;</label>
    </div>
    <div class="input">
        <input type="number" id="size" name="size" min="1" max="10000" step="1" required>
        <span class="error" aria-live="polite"></span>
    </div>
    <div class="product_description">
        <p>Please provide size in MB format</p>
    </div>
</div>
<div id="Furniture" class="df_row">
    <div class="iner_row">
        <div class="label">  
            <label for="height">Height &#40;CM&#41;</label>
        </div>
        <div class="input">
            <input type="number" id="height" name="height" min="1" max="1000" step="0.01" required>
            <span class="error" aria-live="polite"></span>
        </div>
    </div>
    <div class="iner_row">
        <div class="label">  
            <label for="width">Width &#40;CM&#41;</label>
        </div>
        <div class="input">
            <input type="number" id="width" name="width" min="1" max="1000" step="0.01" required>
            <span class="error" aria-live="polite"></span>
        </div>
    </div>
    <div class="iner_row">
        <div class="label">  
            <label for="length">Length &#40;CM&#41;</label>
        </div>
        <div class="input">
            <input type="number" id="length"  name="length" min="1" max="1000" step="0.01" required>
            <span class="error" aria-live="polite"></span>
        </div>
    </div>
    <div class="product_description">
        <p>Please provide dimensions in HxWxL format</p>
    </div>
</div>
<div id="Book" class="df_row">
    <div class="label">  
        <label for="weight">Weight &#40;KG&#41;</label>
    </div>
    <div class="input">
        <input type="number" id="weight" name="weight" min="1" max="1000" step="0.1" required>
        <span class="error" aria-live="polite"></span>
    </div>
    <div class="product_description">
        <p>Please provide weight in KG format</p>
    </div>
</div>