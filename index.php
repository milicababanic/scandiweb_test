<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product list</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
	<?php include ('header.php')?>
    <div class="body_container">
        <div class="product_container">
            <ul id="product_list" class="product_list">
                <?php
                    require_once 'database.php';
                    $data = new database();
                    $data->select("test_table", "*");
                    $result = $data->sql;
                    require_once 'php_classes.php';
                ?>
                <?php while ($row = mysqli_fetch_assoc($result)) { ?>
                    <li class="product_wrapper">
                        <input class="delete-checkbox" type="checkbox" value="<?php echo $row['id']; ?>"/>
                        <?php  
                            if($row['type'] == 'Book'){
                                $book = new Book();
                                $book -> set_data($row['type'], $row['sku'], $row['name'], $row['price'], $row['props']); ?>
                                <span><?php echo $book -> get_sku();?></span>
                                <span><?php echo $book -> get_name();?></span>
                                <span><?php echo $book -> get_price();?></span>
                                <span><?php echo $book -> product_props();?></span>
                        <?php 
                            } elseif($row['type'] == 'DVD'){
                                $dvd = new DVD();
                                $dvd -> set_data($row['type'], $row['sku'], $row['name'], $row['price'], $row['props']); ?>
                                <span><?php echo $dvd -> get_sku();?></span>
                                <span><?php echo $dvd -> get_name();?></span>
                                <span><?php echo $dvd -> get_price();?></span>
                                <span><?php echo $dvd -> product_props();?></span>
                        <?php
                            } elseif($row['type'] == 'Furniture'){
                                $furniture = new Furniture();
                                $furniture -> set_data($row['type'], $row['sku'], $row['name'], $row['price'], $row['props']); ?>
                                <span><?php echo $furniture -> get_sku();?></span>
                                <span><?php echo $furniture -> get_name();?></span>
                                <span><?php echo $furniture -> get_price();?></span>
                                <span><?php echo $furniture -> product_props();?></span>
                        <?php }; ?>
                <?php } ?>
            </ul>
        </div>
    </div>
    <script>
		function redirectToProductPage(){
			document.location.href='./add-product.php';
		};
        function deleteProduct(){
            var checkboxes = document.getElementsByClassName('delete-checkbox');
            const id_array = [];
            for(i=0; i<checkboxes.length;i++){
                if (checkboxes[i].checked) {
                    id_array.push(checkboxes[i].value)
                }
            }
            document.location.href=`./delete.php?id=${id_array}`;
         }
    </script>
</body>
<?php include ('footer.php')?>
</html>
