function showForm(element){
    var ids=['DVD','Book','Furniture']
    for(i=0;i<ids.length;i++){
        document.getElementById(ids[i]).style.display = 'none';
    }
    document.getElementById(element.value).style.display = 'flex';
}

function returnToHome(){ 
    document.location.href='./index.php'
}
var saveInput = document.getElementById("save-product-btn")
saveInput.addEventListener('click', function (event) {
    event.preventDefault();
    if(!sku.validity.valid) {
        showErrorText(sku, skuError)
    };
    if(!nameCheck.validity.valid) {
        showErrorText(nameCheck, nameError);
    };
    if(!price.validity.valid || price.value === '') {
        showErrorNumber(price, priceError);
    };
    formForSubmission = document.getElementById("product_form");
    formForSubmission.submit()
});

// FORM DATA VALIDATION
// SKU FIELD
var skuValue = document.getElementById('sku');
var skuError = document.querySelector('#sku + span.error');

skuValue.addEventListener('input', function (event) {
    if (skuValue.validity.valid) {
        skuError.textContent = ''; 
        skuError.className = 'error'; 
    } else {
        showErrorText(skuValue, skuError);
    }
    });
// NAME FIELD
var nameCheck = document.getElementById('name');
var nameError = document.querySelector('#name + span.error');

nameCheck.addEventListener('input', function (event) {
    if (nameCheck.validity.valid) {
        nameError.textContent = ''; 
        nameError.className = 'error'; 
    } else {
        showErrorText(nameCheck, nameError);
    }
    });
// PRICE FIELD
var priceValue = document.getElementById('price');
var priceError = document.querySelector('#price + span.error');

priceValue.addEventListener('input', function (event) {
    if (priceValue.validity.valid) {
        priceError.textContent = ''; 
        priceError.className = 'error'; 
    } else {
        showErrorNumber(priceValue, priceError);
    }
    });
// SIZE FIELD
var sizeValue = document.getElementById('size');
var sizeError = document.querySelector('#size + span.error');

sizeValue.addEventListener('input', function (event) {
    if (sizeValue.validity.valid) {
        sizeError.textContent = ''; 
        sizeError.className = 'error'; 
    } else {
        showErrorNumber(sizeValue, sizeError);
    }
    });
// HEIGHT FIELD
var heightValue = document.getElementById('height');
var heightError = document.querySelector('#height + span.error');

heightValue.addEventListener('input', function (event) {
    if (heightValue.validity.valid) {
        heightError.textContent = ''; 
        heightError.className = 'error'; 
    } else {
        showErrorNumber(heightValue, heightError);
    }
    });
// WIDTH FIELD
var widthValue = document.getElementById('width');
var widthError = document.querySelector('#width + span.error');

widthValue.addEventListener('input', function (event) {
    if (widthValue.validity.valid) {
        widthError.textContent = ''; 
        widthError.className = 'error'; 
    } else {
        showErrorNumber(widthValue, widthError);
    }
    });
// LENGTH FIELD
var lengthValue = document.getElementById('length');
var lengthError = document.querySelector('#length + span.error');

lengthValue.addEventListener('input', function (event) {
    if (lengthValue.validity.valid) {
        lengthError.textContent = ''; 
        lengthError.className = 'error'; 
    } else {
        showErrorNumber(lengthValue, lengthError);
    }
    });
// WEIGHT FIELD
var weightValue = document.getElementById('weight');
var weightError = document.querySelector('#weight + span.error');

weightValue.addEventListener('input', function (event) {
    if (weightValue.validity.valid) {
        weightError.textContent = ''; 
        weightError.className = 'error'; 
    } else {
        showErrorNumber(weightValue, weightError);
    }
    });
// COMMON FUNCTION FOR TEXT VALIDATION
function showErrorText(first, second) {
    if(first.validity.valueMissing) {
        second.textContent = `Please, submit required data!`;
    } else if(first.validity.tooShort) {
        second.textContent = `Please, provide the data of indicated type! Text should be at least ${ first.minLength } characters; you entered ${ first.value.length }.`;
    }
    second.className = 'error active';
    }

// COMMON FUNCTION FOR NUMBER VALIDATION
function showErrorNumber(first, second) {
    if(first.validity.valueMissing || first.value === '') {
        second.textContent = `Please, submit required data!`;
    } else if(first.validity.rangeOverflow || first.validity.rangeUnderflow) {
        second.textContent = `Please, provide the data of indicated type!`;
    }
    second.className = 'error active';
    };